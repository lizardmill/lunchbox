const {
    getFullSchedule,
    backMeow,
    survey,
    invalidMessageTxt,
    whoIsOut,
    wtfMate,
    motherMayI,
    outPlz,
    deleteMe,
} = require('./controller');

// Inject mock stringtotimestamp dependency for use in controller unit tests
// see __mocks__/stringtotimestamp.js
jest.mock('./stringtotimestamp');

const getDynamoDBClientMock = require('./test-fixtures/dynamodb');
const {
    referenceTimestamp,
    minutes,
} = require('./test-fixtures/reference-time');

// @todo: move constants to mockClient ("mockExampleData, mockExampleErrorChicken, mockExampleErrorInvalidDocumentPath")
const exampleData = {
    Item: {
        date: '6/10/2018',
        heroes: {
            // Standard use case: hero went to lunch and is already back
            // (slight) edge case: hero took longer than 30 minutes on lunch
            heroID1: {
                timeOut: referenceTimestamp - minutes(45),
                timeBack: referenceTimestamp - minutes(10),
                isOut: false
            },
            // Standard use case: hero is on lunch, and not back yet
            // Edge case: hero has taken longer than 30 minutes to return
            heroID2: {
                timeOut: referenceTimestamp - minutes(32),
                // timeBack: nope,
                isOut: true
            },
            // Edge case: hero scheduled lunch, time has passed, hero never marked themselves away
            heroID3: {
                timeOut: referenceTimestamp - minutes(26),
                // timeBack: nope,
                isOut: false
            },
            // Standard use case: hero scheduled lunch, time is in future (hero has not left yet)
            heroID4: {
                timeOut: referenceTimestamp + minutes(10),
                // timeBack: nope,
                isOut: false
            }
        }
    }
};
const exampleError = new Error("Error on line \"+CHICKEN+\": expected 'chicken'");
const invalidDocumentPathError = new Error("The document path provided in the update expression is invalid for update");
const heroIDLiz = "U68RCQYH5";
const slackTime = (timestamp) => `<!date^${timestamp}^{time}|cannot_display_time>`;
const getTimefor = (heroID, timeProp = 'timeOut', dbItem = exampleData.Item) => slackTime(dbItem.heroes[heroID][timeProp]);
const getReturnTime = (heroID, timeProp = 'timeOut', dbItem = exampleData.Item) => slackTime(dbItem.heroes[heroID][timeProp]+1800);

describe('getFullSchedule', () => {

    it('should return a promise that resolves', () => {
        expect.assertions(1);
        const mockClient = getDynamoDBClientMock();

        let p = getFullSchedule(mockClient, referenceTimestamp);

        // MOCK CLIENT: Return data from db get call
        mockClient.invokeGetCb(null, exampleData);

        return p.then((result) => {
            const expected =
`<@heroID1> scheduled lunch from ${getTimefor('heroID1')} to ${getTimefor('heroID1', 'timeBack')}.
<@heroID2> went on lunch at ${getTimefor('heroID2')} and should be back at ${getReturnTime('heroID2')}.
<@heroID3> scheduled lunch at ${getTimefor('heroID3')}, but has not marked themselves away.
<@heroID4> has lunch scheduled at ${getTimefor('heroID4')}.`;
            expect(result).toEqual(expected);
        });
    });

    it('should return a promise that resolves, even if err', () => {
        expect.assertions(1);
        const mockClient = getDynamoDBClientMock();

        let p = getFullSchedule(mockClient, referenceTimestamp);

        // MOCK CLIENT: Return error from db get call
        mockClient.invokeGetCb(exampleError, null);

        const expected = `There was a problem! Tell @Liz: Error on line \"+CHICKEN+\": expected 'chicken'.`;

        return p.then((result) => {
            expect(result).toEqual(expected);
        });
    });

});

describe('backMeow', () => {

    it('should return a promise that resolves', () => {
        expect.assertions(1);
        const mockClient = getDynamoDBClientMock();

        // Hero is currently out
        let p = backMeow(mockClient, referenceTimestamp, 'heroID2');

        // MOCK CLIENT: Return data from db get call
        mockClient.invokeGetCb(null, exampleData);

        // MOCK CLIENT: Return success from db update call
        mockClient.invokeUpdateCb(null, 'meow');

        return p.then((result) => {
            const expected = `Welcome back, <@heroID2>.`;
            expect(result).toEqual(expected);
        });
    });

    it('should return a promise that resolves, even if DB err during get', () => {
        expect.assertions(1);
        const mockClient = getDynamoDBClientMock();

        // Hero is currently out
        let p = backMeow(mockClient, referenceTimestamp, 'heroID2');

        // MOCK CLIENT: Return err from db get call
        mockClient.invokeGetCb(exampleError, null);

        return p.then((result) => {
            const expected = `There was a problem! Tell @Liz: Error on line \"+CHICKEN+\": expected 'chicken'.`;
            expect(result).toEqual(expected);
        });
    });

    it('should return a promise that resolves, even if DB err during update', () => {
        expect.assertions(1);
        const mockClient = getDynamoDBClientMock();

        // Hero is currently out
        let p = backMeow(mockClient, referenceTimestamp, 'heroID2');

        // MOCK CLIENT: Return data from db get call
        mockClient.invokeGetCb(null, exampleData);

        // MOCK CLIENT: Return err from db update call
        mockClient.invokeUpdateCb(exampleError, null);

        return p.then((result) => {
            const expected = `There was a problem! Tell @Liz: Error on line \"+CHICKEN+\": expected 'chicken'.`;
            expect(result).toEqual(expected);
        });
    });

    it('should not resolve until the database is updated', () => {
        expect.assertions(2);
        const mockClient = getDynamoDBClientMock();
        const timeStart = Date.now();
        const delay = 20; // ms

        let p = backMeow(mockClient, referenceTimestamp, 'heroID2');

        // MOCK CLIENT: Return data from db get call
        mockClient.invokeGetCb(null, exampleData);

        // MOCK CLIENT: Wait some time before returning from db update call
        setTimeout(
            () => mockClient.invokeUpdateCb(null, {status: 'yea I did the thing'}),
            delay
        );

        return p.then((result) => {
            const timeEnd = Date.now();
            const expected = `Welcome back, <@heroID2>.`;
            expect(result).toEqual(expected);
            // Promise should not have resolved until database update is complete
            expect(timeEnd - timeStart).toBeGreaterThanOrEqual(delay);
        });
    });

    it('should not let you mark yourself back if you are already back', () => {
        expect.assertions(1);
        const mockClient = getDynamoDBClientMock();

        // This hero is already back from lunch...
        let p = backMeow(mockClient, referenceTimestamp, 'heroID1');

        // MOCK CLIENT: Return data from db get call
        mockClient.invokeGetCb(null, exampleData);

        return p.then((result) => {
            const expected = `Hm wat? You already marked yourself back at ${getTimefor('heroID1', 'timeBack')}.`;
            expect(result).toEqual(expected);
        });
    });

    it('should do (something?) if you never marked yourself away', () => {
        expect.assertions(1);
        const mockClient = getDynamoDBClientMock();

        // Who is this joker?
        let p = backMeow(mockClient, referenceTimestamp, 'heroID3');

        // MOCK CLIENT: Return data from db get call
        mockClient.invokeGetCb(null, exampleData);

        return p.then((result) => {
            const expected = `I have an entry for you, but you never left. You feeling okay?`;
            expect(result).toEqual(expected);
        });
    });

    it('should not let you mark yourself back if you aren\'t on the schedule', () => {
        expect.assertions(1);
        const mockClient = getDynamoDBClientMock();

        // Who is this joker?
        let p = backMeow(mockClient, referenceTimestamp, 'heroIDnotout');

        // MOCK CLIENT: Return data from db get call
        mockClient.invokeGetCb(null, exampleData);

        return p.then((result) => {
            const expected = `I don't have a lunch scheduled for you.`;
            expect(result).toEqual(expected);
        });
    });

});

describe('whoIsOut', () => {

    it('should return a promise that resolves', () => {
        expect.assertions(1);
        const mockClient = getDynamoDBClientMock();

        let p = whoIsOut(mockClient, referenceTimestamp);

        // MOCK CLIENT: Return data from db get call
        mockClient.invokeGetCb(null, exampleData);

        return p.then((result) => {
            const expected = `<@heroID2> went for lunch at ${getTimefor('heroID2')} and should be back at <!date^1528648080^{time}|cannot_display_time>.`;

            expect(result).toEqual(expected);
        });
    });

    it('should return a promise that resolves, even if err', () => {
        expect.assertions(1);
        const mockClient = getDynamoDBClientMock();

        let p = whoIsOut(mockClient, referenceTimestamp);

        // MOCK CLIENT: Return error from db get call
        mockClient.invokeGetCb(exampleError, null);

        const expected = `There was a problem! Tell @Liz: Error on line \"+CHICKEN+\": expected 'chicken'.`;

        return p.then((result) => {
            expect(result).toEqual(expected);
        });
    });

});

describe('deleteMe', () => {

    it('should return a promise that resolves', () => {
        expect.assertions(1);
        const mockClient = getDynamoDBClientMock();

        // Hero has lunch scheduled in 10 minutes
        let p = deleteMe(mockClient, referenceTimestamp, 'heroID4');

        // MOCK CLIENT: Return data from db get call
        mockClient.invokeGetCb(null, exampleData);

        // MOCK CLIENT: Return success from db update call
        mockClient.invokeUpdateCb(null, {status: 'yep okay done'});

        return p.then((result) => {
            const expected = `Your lunch entry has been deleted.`;
            expect(result).toEqual(expected);
        });
    });

    it('should return a promise that resolves, even if DB err during get', () => {
        expect.assertions(1);
        const mockClient = getDynamoDBClientMock();

        // Hero has lunch scheduled in 10 minutes
        let p = deleteMe(mockClient, referenceTimestamp, 'heroID4');

        // MOCK CLIENT: Return err from db get call
        mockClient.invokeGetCb(exampleError, null);

        return p.then((result) => {
            const expected = `There was a problem! Tell @Liz: Error on line \"+CHICKEN+\": expected 'chicken'.`;
            expect(result).toEqual(expected);
        });
    });

    it('should return a promise that resolves, even if DB err during update', () => {
        expect.assertions(1);
        const mockClient = getDynamoDBClientMock();

        // Hero has lunch scheduled in 10 minutes
        let p = deleteMe(mockClient, referenceTimestamp, 'heroID4');

        // MOCK CLIENT: Return data from db get call
        mockClient.invokeGetCb(null, exampleData);

        // MOCK CLIENT: Return err from db update call
        mockClient.invokeUpdateCb(exampleError, null);

        return p.then((result) => {
            const expected = `There was a problem! Tell @Liz: Error on line \"+CHICKEN+\": expected 'chicken'.`;
            expect(result).toEqual(expected);
        });
    });

    it('should not resolve until the database is updated', () => {
        expect.assertions(2);
        const mockClient = getDynamoDBClientMock();
        const timeStart = Date.now();
        const delay = 20; // ms

        // Hero has lunch scheduled in 10 minutes
        let p = deleteMe(mockClient, referenceTimestamp, 'heroID4');

        // MOCK CLIENT: Return data from db get call
        mockClient.invokeGetCb(null, exampleData);

        // MOCK CLIENT: Wait some time before returning from db update call
        setTimeout(
            () => mockClient.invokeUpdateCb(null, {status: 'yea I did the thing'}),
            delay
        );

        return p.then((result) => {
            const timeEnd = Date.now();
            const expected = `Your lunch entry has been deleted.`;
            expect(result).toEqual(expected);
            // Promise should not have resolved until database update is complete
            expect(timeEnd - timeStart).toBeGreaterThanOrEqual(delay);
        });
    });

    it('should let you delete your entry if you have already left', () => {
        expect.assertions(1);
        const mockClient = getDynamoDBClientMock();

        // Hero left a while ago
        let p = deleteMe(mockClient, referenceTimestamp, 'heroID2');

        // MOCK CLIENT: Return data from db get call
        mockClient.invokeGetCb(null, exampleData);
        // MOCK CLIENT: Update data from db get call
        mockClient.invokeUpdateCb(null, exampleData);

        return p.then((result) => {
            const expected = `Your lunch entry has been deleted.`;
            expect(result).toEqual(expected);
        });
    });

    it('should let you delete your entry if you have already left and returned', () => {
        expect.assertions(1);
        const mockClient = getDynamoDBClientMock();

        // Hero had their lunch and is already back
        let p = deleteMe(mockClient, referenceTimestamp, 'heroID1');

        // MOCK CLIENT: Return data from db get call
        mockClient.invokeGetCb(null, exampleData);
        // MOCK CLIENT: Update data from db get call
        mockClient.invokeUpdateCb(null, exampleData);

        return p.then((result) => {
            const expected = `Your lunch entry has been deleted.`;
            expect(result).toEqual(expected);
        });
    });

    it('should let you delete your entry if you never left', () => {
        expect.assertions(1);
        const mockClient = getDynamoDBClientMock();

        // Hero scheduled lunch a while ago, but couldn't go because they were busy
        let p = deleteMe(mockClient, referenceTimestamp, 'heroID3');

        // MOCK CLIENT: Return data from db get call
        mockClient.invokeGetCb(null, exampleData);

        // MOCK CLIENT: Return success from db update call
        mockClient.invokeUpdateCb(null, {status: 'yea I did the thing'});

        return p.then((result) => {
            const expected = `Your lunch entry has been deleted.`;
            expect(result).toEqual(expected);
        });
    });

    it('should reset all properties', () => {
        const mockClient = getDynamoDBClientMock();

        // Hero scheduled lunch a while ago, but couldn't go because they were busy
        let p = deleteMe(mockClient, referenceTimestamp, 'heroID3');

        // MOCK CLIENT: Return data from db get call
        mockClient.invokeGetCb(null, exampleData);

        const updateParams = mockClient.getParamObject('update');
        expect(updateParams.ExpressionAttributeValues[':object']).toEqual({
            isOut: false,
            timeOut: 0,
            timeBack: 0,
        });
    });

});

describe('motherMayI', () => {

    it('should return a promise that resolves', () => {
        expect.assertions(1);
        const mockClient = getDynamoDBClientMock();
        const exampleDataLocal = {
            Item: {
                date: '6/10/2018',
                heroes: {
                    // Standard use case: hero went to lunch and is already back
                    // (slight) edge case: hero took longer than 30 minutes on lunch
                    heroID1: {
                        timeOut: referenceTimestamp - minutes(45),
                        timeBack: referenceTimestamp - minutes(10),
                        isOut: false
                    },
                }
            }
        };

        // Hero is not on schedule
        let p = motherMayI(mockClient, referenceTimestamp, 'heroIDnotout', 'now');

        // MOCK CLIENT: Return data from db get call
        mockClient.invokeGetCb(null, exampleDataLocal);

        // MOCK CLIENT: Return success from db update call
        mockClient.invokeUpdateCb(null, {status: 'yep okay done'});

        return p.then((result) => {
            const expected = `Go for it, <@heroIDnotout>! Have a good lunch!`;
            expect(result).toEqual(expected);
        });
    });

    it('should return a promise that resolves, even if err during get', () => {
        expect.assertions(1);
        const mockClient = getDynamoDBClientMock();

        // Hero is not on schedule
        let p = motherMayI(mockClient, referenceTimestamp, 'heroIDnotout', 'now');

        // MOCK CLIENT: Return error from db get call
        mockClient.invokeGetCb(exampleError, null);

        return p.then((result) => {
            const expected = `There was a problem! Tell @Liz: Error on line \"+CHICKEN+\": expected 'chicken'.`;
            expect(result).toEqual(expected);
        });
    });

    it('should return a promise that resolves, even if err during update', () => {
        expect.assertions(1);
        const mockClient = getDynamoDBClientMock();
        const exampleDataLocal = {
            Item: {
                date: '6/10/2018',
                heroes: {
                    // Standard use case: hero went to lunch and is already back
                    // (slight) edge case: hero took longer than 30 minutes on lunch
                    heroID1: {
                        timeOut: referenceTimestamp - minutes(45),
                        timeBack: referenceTimestamp - minutes(10),
                        isOut: false
                    },
                }
            }
        };

        // Hero is not on schedule
        let p = motherMayI(mockClient, referenceTimestamp, 'heroIDnotout', 'now');

        // MOCK CLIENT: Return data from db get call
        mockClient.invokeGetCb(null, exampleDataLocal);

        // MOCK CLIENT: Return error from db update call
        mockClient.invokeUpdateCb(exampleError, null);

        return p.then((result) => {
            const expected = `There was a problem! Tell @Liz: Error on line \"+CHICKEN+\": expected 'chicken'.`;
            expect(result).toEqual(expected);
        });
    });

    it('should not resolve until the database is updated', () => {
        expect.assertions(2);
        const mockClient = getDynamoDBClientMock();
        const timeStart = Date.now();
        const delay = 20; // ms
        const exampleDataLocal = {
            Item: {
                date: '6/10/2018',
                heroes: {
                    // Standard use case: hero went to lunch and is already back
                    // (slight) edge case: hero took longer than 30 minutes on lunch
                    heroID1: {
                        timeOut: referenceTimestamp - minutes(45),
                        timeBack: referenceTimestamp - minutes(10),
                        isOut: false
                    },
                }
            }
        };

        // Standard use case: a different hero wants to go to lunch
        let p = motherMayI(mockClient, referenceTimestamp, 'heroIDnotout', 'now');

        // MOCK CLIENT: Return data from db get call
        mockClient.invokeGetCb(null, exampleDataLocal);

        // MOCK CLIENT: Wait some time before returning from db update call
        setTimeout(
            () => mockClient.invokeUpdateCb(null, {status: 'yea I did the thing'}),
            delay
        );

        return p.then((result) => {
            const timeEnd = Date.now();
            const expected = `Go for it, <@heroIDnotout>! Have a good lunch!`;
            expect(result).toEqual(expected);
            // Promise should not have resolved until database update is complete
            expect(timeEnd - timeStart).toBeGreaterThanOrEqual(delay);
        });
    });

    it('should not let you go if you already went', () => {
        expect.assertions(1);
        const mockClient = getDynamoDBClientMock();
        const exampleDataLocal = {
            Item: {
                date: '6/10/2018',
                heroes: {
                    // Standard use case: hero went to lunch and is already back
                    // (slight) edge case: hero took longer than 30 minutes on lunch
                    heroID1: {
                        timeOut: referenceTimestamp - minutes(45),
                        timeBack: referenceTimestamp - minutes(10),
                        isOut: false
                    },
                }
            }
        };

        // Edge case: Hero already went to lunch
        let p = motherMayI(mockClient, referenceTimestamp, 'heroID1', 'now');

        // MOCK CLIENT: Return data from db get call
        mockClient.invokeGetCb(null, exampleDataLocal);

        return p.then((result) => {
            const expected = `Yeah nah. You already went to lunch, mate.`;
            expect(result).toEqual(expected);
        });
    });

    it('should not let you go if you are away', () => {
        expect.assertions(1);
        const mockClient = getDynamoDBClientMock();
        const exampleDataLocal = {
            Item: {
                date: '6/10/2018',
                heroes: {
                    // Standard use case: hero is out on lunch
                    heroID1: {
                        timeOut: referenceTimestamp - minutes(15),
                        // timeBack: nope,
                        isOut: true
                    },
                }
            }
        };

        // Edge case: Hero already went to lunch
        let p = motherMayI(mockClient, referenceTimestamp, 'heroID1', 'now');

        // MOCK CLIENT: Return data from db get call
        mockClient.invokeGetCb(null, exampleDataLocal);

        return p.then((result) => {
            const expected = `You are already marked away, yo.`;
            expect(result).toEqual(expected);
        });
    });

    it('should let you go if it is almost time', () => {
        expect.assertions(1);
        const mockClient = getDynamoDBClientMock();
        const exampleDataLocal = {
            Item: {
                date: '6/10/2018',
                heroes: {
                    // Standard use case: hero is scheduled for lunch, in two minutes
                    heroID1: {
                        timeOut: referenceTimestamp + minutes(2),
                        // timeBack: nope,
                        isOut: false
                    },
                }
            }
        };

        // Standard use case: Hero is scheduled and it is almost time
        let p = motherMayI(mockClient, referenceTimestamp, 'heroID1', 'now');

        // MOCK CLIENT: Return data from db get call
        mockClient.invokeGetCb(null, exampleDataLocal);

        // MOCK CLIENT: Return success from db update call
        mockClient.invokeUpdateCb(null, {status: 'yep ok'});

        return p.then((result) => {
            const expected = `Go for it, <@heroID1>! Have a good lunch!`;
            expect(result).toEqual(expected);
        });
    });

    it('should override a scheduled lunch', () => {
        expect.assertions(2);
        const mockClient = getDynamoDBClientMock();
        const exampleDataLocal = {
            Item: {
                date: '6/10/2018',
                heroes: {
                    // Standard use case: hero is scheduled for lunch, in a long time
                    heroID1: {
                        timeOut: referenceTimestamp + minutes(120),
                        // timeBack: nope,
                        isOut: false
                    },
                }
            }
        };

        // Standard use case: Hero would like to go now
        let p = motherMayI(mockClient, referenceTimestamp, 'heroID1', 'plz');

        // MOCK CLIENT: Return data from db get call
        mockClient.invokeGetCb(null, exampleDataLocal);

        // MOCK CLIENT: Return success from db update call
        mockClient.invokeUpdateCb(null, {status: 'yep ok'});

        // DB should get updated to reflect that hero is now out
        const updateParams = mockClient.getParamObject('update');
        expect(updateParams.ExpressionAttributeValues[':object']).toEqual({
            isOut: true,
            timeOut: referenceTimestamp,
            timeBack: 0,
        });

        return p.then((result) => {
            const expected = `Go for it, <@heroID1>! Have a good lunch!`;
            expect(result).toEqual(expected);
        });
    });

    it('should not let you go early if it means 3 people will be out (wait a bit!)', () => {
        expect.assertions(1);
        const mockClient = getDynamoDBClientMock();
        const exampleDataLocal = {
            Item: {
                date: '6/10/2018',
                heroes: {
                    // Standard use case: hero is out
                    heroID1: {
                        timeOut: referenceTimestamp - minutes(25),
                        // timeBack: nope,
                        isOut: true
                    },
                    // Standard use case: hero is out
                    heroID2: {
                        timeOut: referenceTimestamp - minutes(20),
                        // timeBack: nope,
                        isOut: true
                    },
                    // Standard use case: hero is scheduled soon
                    heroID3: {
                        timeOut: referenceTimestamp + minutes(8),
                        // timeBack: nope,
                        isOut: false
                    },
                }
            }
        };

        // Standard use case: Hero would like to go now, except two are currently out!
        let p = motherMayI(mockClient, referenceTimestamp, 'heroID3', 'plz');

        // MOCK CLIENT: Return data from db get call
        mockClient.invokeGetCb(null, exampleDataLocal);

        return p.then((result) => {
            const expected = `I'm sorry, <@heroID3>. There will be more than two heroes out at a time if you go now. Wait until your time! If you really must run, check with your lead first.`;
            expect(result).toEqual(expected);
        });
    });

    it('should let you schedule a time', () => {
        expect.assertions(2);
        const mockClient = getDynamoDBClientMock();
        const exampleDataLocal = {
            Item: {
                date: '6/10/2018',
                heroes: {
                    // Standard use case: hero is scheduled for lunch, in two minutes
                    heroID1: {
                        timeOut: referenceTimestamp + minutes(2),
                        // timeBack: nope,
                        isOut: false
                    },
                }
            }
        };

        // Standard use case: A different hero would like to schedule lunch
        let p = motherMayI(mockClient, referenceTimestamp, 'heroIDnotout', 'plz 12:30');

        // MOCK CLIENT: Return data from db get call
        mockClient.invokeGetCb(null, exampleDataLocal);

        // MOCK CLIENT: Return success from db update call
        mockClient.invokeUpdateCb(null, {status: 'yep ok'});

        // DB should get updated with hero's requested lunch time
        const updateParams = mockClient.getParamObject('update');
        expect(updateParams.ExpressionAttributeValues[':object']).toEqual({
            isOut: false,
            timeOut: referenceTimestamp + minutes(120), // reference timestamp is 10:30 + two hours = 12:30
            timeBack: 0,
        });

        return p.then((result) => {
            const expected = `Okay <@heroIDnotout>, I have you scheduled for ${slackTime(referenceTimestamp + minutes(120))}.`;
            expect(result).toEqual(expected);
        });
    });

    it('should detect conflicts', () => {
        expect.assertions(1);
        const mockClient = getDynamoDBClientMock();
        const exampleDataLocal = {
            Item: {
                date: '6/10/2018',
                heroes: {
                    // Standard use case: hero is currently out
                    heroID1: {
                        timeOut: referenceTimestamp - minutes(10),
                        // timeBack: nope,
                        isOut: true
                    },
                    // Standard use case: hero is scheduled soon
                    heroID2: {
                        timeOut: referenceTimestamp + minutes(2),
                        // timeBack: nope,
                        isOut: false
                    },
                }
            }
        };

        // Standard use case: A different hero would like to schedule lunch, but that would lead to 3 being out!
        let p = motherMayI(mockClient, referenceTimestamp + minutes(10), 'heroIDnotout', 'plz');

        // MOCK CLIENT: Return data from db get call
        mockClient.invokeGetCb(null, exampleDataLocal);

        return p.then((result) => {
            const expected = `I'm sorry, <@heroIDnotout>. There will be more than two heroes out at a time if you go now. If you really must run, check with your lead first.`;
            expect(result).toEqual(expected);
        });
    });

    it('should not let you reschedule', () => {
        expect.assertions(1);
        const mockClient = getDynamoDBClientMock();
        const exampleDataLocal = {
            Item: {
                date: '6/10/2018',
                heroes: {
                    // Standard use case: hero is scheduled for lunch, in two hours
                    heroID1: {
                        timeOut: referenceTimestamp + minutes(120),
                        // timeBack: nope,
                        isOut: false
                    },
                }
            }
        };

        // Standard use case: Hero would like to go earlier
        let p = motherMayI(mockClient, referenceTimestamp, 'heroID1', 'plz 11:30');

        // MOCK CLIENT: Return data from db get call
        mockClient.invokeGetCb(null, exampleDataLocal);

        return p.then((result) => {
            const expected = `Yeah nah. You are already scheduled for ${slackTime(referenceTimestamp + minutes(120))}.`;
            expect(result).toEqual(expected);
        });
    });

    it('should complain if user input is invalid', () => {
        expect.assertions(1);
        const mockClient = getDynamoDBClientMock();
        const exampleDataLocal = {
            Item: {
                date: '6/10/2018',
                heroes: {
                    //
                }
            }
        };

        // Standard use case: Hero is drunk
        let p = motherMayI(mockClient, referenceTimestamp, 'heroIDnotout', 'plz cats');

        // MOCK CLIENT: Return data from db get call
        mockClient.invokeGetCb(null, exampleDataLocal);

        // controller _shouldn't_ call update, but just in case it did...
        try {
            mockClient.invokeUpdateCb(null, {status: 'wtf?'});
        } catch (err) {
            // pass
        }

        return p.then((result) => {
            const expected = `I wasn't able to understand what time you are trying to request for lunch. Please specify a time in the format HH:MM am/pm.`;
            expect(result).toEqual(expected);
        });
    });

});

describe('wtfMate', () => {

    it('should return a string with the slack id number converted into human readable username', () => {
        expect(
            wtfMate(heroIDLiz)
        ).toEqual(
            "Hi <@"+heroIDLiz+">. Lunchbox is a SG Support lunch bot. Request a lunch spot in advance by asking nicely (like `/supportlunch plz 1:30pm`) or see if you can just go now with `/supportlunch plz`. When you're done with lunch,remember to mark yourself as `/supportlunch bek` or you'll ruin everything. See who's out now with `/supportlunch who` and see everyone who's scheduled or gone to lunch so far today with `/supportlunch all`."
        )
    });

})
