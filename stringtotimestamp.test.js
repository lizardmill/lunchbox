const { convert, getMSTDateString } = require('./stringtotimestamp');
const timezone_mock = require('timezone-mock');
const { getHoursMST } = require('./utils/timezones');
const { info } = require('./utils/logger')('stringtotimestamp.test');
const { minutes } = require('./test-fixtures/reference-time');
const spacetime = require('spacetime');

// Convert a timestamp to a padded hH:MM string
const getTimeString = (hour, minutes) => `${hour}:${minutes < 10 ? '0' + minutes : minutes}`;
// Get the current time, with seconds and MS zeroed
const getCurrentTime = () => {
    const currentTime = new Date();
    currentTime.setUTCSeconds(0);
    currentTime.setUTCMilliseconds(0);
    return currentTime;
};
// Sun Jul  8 00:00:01 MDT 2018
const jul8ZeroHours = 1531029601;
// Sun Jul  8 23:59:59 MDT 2018
const jul8TwentyThreeHours = 1531115999;

describe('getMSTDateString', () => {

    it('will use MST to generate todays date string, regardless of server timezone', () => {
        ['US/Pacific', 'US/Eastern'].map(timezone => {
            timezone_mock.register(timezone);
            info(`Expecting ${jul8ZeroHours} to equal '7/8/2018'`);
            expect(
                getMSTDateString(jul8ZeroHours)
            ).toEqual(
                '7/8/2018'
            );
            info(`Expecting ${jul8TwentyThreeHours} to equal '7/8/2018'`);
            expect(
                getMSTDateString(jul8TwentyThreeHours)
            ).toEqual(
                '7/8/2018'
            );
            timezone_mock.unregister();
        });
    });

});

describe('convert', () => {

    it('will convert a string to a timestamp', () => {
        const currentTime = getCurrentTime();

        let requestHour = getHoursMST(currentTime);
        let requestMinutes = currentTime.getUTCMinutes();

        const inputValue = getTimeString(requestHour, requestMinutes);
        const expectedTimestamp = currentTime/1000;

        info(`Expecting ${inputValue} to equal: ${expectedTimestamp}`);

        expect(
            convert(inputValue)
        ).toEqual(
            expectedTimestamp
        );
    });

    it('accepts future times', () => {
        const currentTime = getCurrentTime();

        let offset = 1;

        let requestHour = getHoursMST(currentTime) + offset;
        let requestMinutes = currentTime.getUTCMinutes();

        const inputValue = getTimeString(requestHour, requestMinutes);
        const expectedTimestamp = (currentTime/1000) + minutes(60 * offset);

        info(`Expecting ${inputValue} to equal: ${expectedTimestamp}`);

        expect(
            convert(inputValue)
        ).toEqual(
            expectedTimestamp
        );
    });

    it('accepts past times', () => {
        const currentTime = getCurrentTime();

        let offset = -1;

        let requestHour = getHoursMST(currentTime) + offset;
        let requestMinutes = currentTime.getUTCMinutes();

        const inputValue = getTimeString(requestHour, requestMinutes);
        const expectedTimestamp = (currentTime/1000) + minutes(60 * offset);

        info(`Expecting ${inputValue} to equal: ${expectedTimestamp}`);

        expect(
            convert(inputValue)
        ).toEqual(
            expectedTimestamp
        );
    });

    it('accepts 12h time format, with suffix am/pm or a/p', () => {
        const currentTime = getCurrentTime();

        let offset = 0;

        let requestHour = getHoursMST(currentTime);
        let requestMinutes = currentTime.getUTCMinutes();

        // we are already past noon (eg 13 or 14 o'clock). Let's schedule something for "1" or "2" o'clock
        if (requestHour > 12) {
            requestHour -= 12;
        }
        // we are not yet past noon - let's request 1pm (calculate offset for expectedTimestamp)
        else {
            offset = 13 - requestHour;
            requestHour = 1;
        }

        const inputValue = getTimeString(requestHour, requestMinutes) + 'PM';
        const expectedTimestamp = (currentTime/1000) + minutes(60 * offset);
        info(`Expecting ${inputValue} to equal: ${expectedTimestamp}`);

        expect(
            convert(inputValue)
        ).toEqual(
            expectedTimestamp
        );

        const inputValue2 = getTimeString(requestHour, requestMinutes) + 'p';
        info(`Expecting ${inputValue2} to equal: ${expectedTimestamp}`);

        expect(
            convert(inputValue2)
        ).toEqual(
            expectedTimestamp
        );
    });


    it('accepts any time of day, and works in any server timezone', () => {
        const currentTime = getCurrentTime();

        ['US/Pacific', 'US/Eastern', 'UTC'].map(timezone => {
            timezone_mock.register(timezone);

            const offset = getHoursMST(currentTime);
            let requestMinutes = currentTime.getUTCMinutes();

            [2,8,12,16,18,20].map(requestHour => {
                let inputValue = getTimeString(requestHour, requestMinutes);
                let expectedTimestamp = (currentTime/1000) + minutes(60 * (requestHour - offset));
                info(`Expecting ${inputValue} to equal: ${expectedTimestamp}, with server timezone == ${timezone}`);
                expect(
                    convert(inputValue)
                ).toEqual(
                    expectedTimestamp
                );
            });

            timezone_mock.unregister();
        });

    });

    it('works regardless of the current time of day, in any server timezone', () => {
        const currentTime = getCurrentTime();

        let requestHour = getHoursMST(currentTime);
        let requestMinutes = currentTime.getUTCMinutes();

        const inputValue = getTimeString(requestHour, requestMinutes);
        const expectedTimestamp = (currentTime/1000);
        ['US/Pacific', 'US/Eastern', 'UTC'].map(timezone => {
            timezone_mock.register(timezone);

            [2,8,12,16,18,20].map(hour => {
                // Set current MST hour
                currentTime.setUTCHours(currentTime.getUTCHours() - getHoursMST(currentTime) + hour)

                info(`Expecting ${inputValue} to equal: ${expectedTimestamp}, when current time is set to ${getMSTDateString(currentTime/1000)} ${getHoursMST(currentTime)}:${currentTime.getUTCMinutes()} MST, server timezone ${timezone}`);



                expect(
                    convert(inputValue, spacetime(currentTime, 'America/Denver'))
                ).toEqual(
                    expectedTimestamp
                );
            });

            timezone_mock.unregister();
        });
    });

    it('returns NaN if user input is invalid', () => {
        expect(
            convert('fishsticks')
        ).toEqual(
            NaN
        );

        expect(
            convert(99)
        ).toEqual(
            NaN
        );

        expect(
            convert('')
        ).toEqual(
            NaN
        );
    });

});
