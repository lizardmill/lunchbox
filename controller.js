const db = require("./db.js");
const bl = require("./businesslogic.js")
const dataparser = require("./dataparser.js");
const stringToTime = require("./stringtotimestamp.js");
const { log, error } = require('./utils/logger')('controller');

//all
function getFullSchedule(docClient, currentTime) {
    return new Promise(function(resolve,reject) {
       db.getItemsDB(docClient, currentTime, function(err, heroList){
           if (err) {
               resolve(handleErrors(err));
           } else {
               resolve(dataparser.parseGetAllData(heroList, currentTime));
           }
       });
   })
}

//bek
function backMeow(docClient, currentTime, heroID){
    // set params - needed params are heroID, timeBack and isHeroOut
    let heroReturnTime = currentTime;
    // check to see if they already have an entry
    return new Promise(function(resolve, reject) {
        db.getItemsDB(docClient, currentTime, function(err, heroList) {
            if (err) {
                resolve(handleErrors(err));
            }
            // if the hero has an entry, add "timeBack" to hero's object. isOut:false will happen automatically
            if (heroList && heroList[heroID] && heroList[heroID].isOut) {
                heroDataValues = {
                    heroID: heroID,
                    timeOut: heroList[heroID].timeOut,
                    timeBack: heroReturnTime,
                    isOut: false
                }
                //log("heroDataValues is:" +JSON.stringify(heroDataValues));
                db.updateItemDB(docClient, currentTime, heroDataValues, function(err, data){
                    if (err) {
                        resolve(handleErrors(err));
                    } else {
                        resolve("Welcome back, <@"+heroID+">.");
                    }
                });
            } else if (heroList && heroList[heroID] && !heroList[heroID].isOut && heroList[heroID].timeBack){
                resolve("Hm wat? You already marked yourself back at " + dataparser.formatTimestamp(heroList[heroID].timeBack) + ".");
            } else if (heroList && heroList[heroID] && !heroList[heroID].isOut) {
                resolve("I have an entry for you, but you never left. You feeling okay?");
            }
            else {
                resolve("I don't have a lunch scheduled for you.");
            }
        })
    });
}

//feedback
function survey() {
    return "Please let me know what you think! https://www.surveygizmo.com/s3/4296093/Lunchbox-App-Feedback";
}

//invalid message messageText
function invalidMessageTxt(messageText) {
    return "I'm sorry, I don't understand \""+ messageText +"\" . Check `/supportlunch what` for a list of valid commands.";
}

//who
function whoIsOut(docClient, currentTime) {
    //log("controller");
    return new Promise(function(resolve,reject) {
        db.getItemsDB(docClient, currentTime, function(err, heroList){
            //log("When /who is called, the heroList is: "+JSON.stringify(heroList));
            if (err) {
                resolve(handleErrors(err));
            } else {
                resolve(dataparser.parseGetOutData(heroList));
            }
        });
    });
}

//what
function wtfMate(heroID) {
    return "Hi <@"+heroID+">. Lunchbox is a SG Support lunch bot. Request a lunch spot in advance by asking nicely (like `/supportlunch plz 1:30pm`) or see if you can just go now with `/supportlunch plz`. When you're done with lunch,remember to mark yourself as `/supportlunch bek` or you'll ruin everything. See who's out now with `/supportlunch who` and see everyone who's scheduled or gone to lunch so far today with `/supportlunch all`.";
}

//scheduling and marking oneself as out. to do: rename motherMayI
// i may not need message (or may need to rethink) once plz does everything
function motherMayI(docClient, currentTime, heroID, message) {
    log("plz running");
    // check if hero has an entry or not
    return new Promise(function(resolve,reject) {
        db.getItemsDB(docClient, currentTime, function(err, heroList) {
            log(message);
            // if the hero is marked as out, let them know
            if (err) {
                log(err);
            }
            let requestedTimeOut = currentTime;
            let outOrNot = true;

            // if it's scheduling in advance, deal with it
            // example message /supportlunch plz 1:30
            if (message.length > 3 && message.startsWith("plz ")) {
                requestedTimeOut = stringToTime.convert(message.slice(4));
                log(requestedTimeOut);
                if (!requestedTimeOut){
                    resolve("I wasn't able to understand what time you are trying to request for lunch. Please specify a time in the format HH:MM am/pm.");
                }
                outOrNot = false;
            }

            //If hero is currenly marked as out, tell them they're already on lunch.
            //-----Could add a reminder as to when they're supposed to be back.
            if (heroList && heroList[heroID] && heroList[heroID].isOut === true) {
                resolve("You are already marked away, yo.");

            // if the hero has an entry, but they already came back from lunch, don't let them go again
            } else if (heroList && heroList[heroID] && heroList[heroID].timeBack) {
                resolve("Yeah nah. You already went to lunch, mate.");

            // if the hero has an entry, but they are trying to reschedule for a different time, don't let them.
            } else if (heroList && heroList[heroID] && heroList[heroID].timeOut !=0 && message.length > 3){
                resolve("Yeah nah. You are already scheduled for " + dataparser.formatTimestamp(heroList[heroID].timeOut) + ".");

            //if the hero had a previously deleted entry, let them reschedule.
            }  else if (heroList && heroList[heroID] && heroList[heroID].timeOut === 0 && message.length > 3){
                log("hero with previously deleted entry is attempting to reschedule");
                if (bl.canHeroGo(heroList, requestedTimeOut, message)) {
                    log("hero can go");
                    // update the document with a new item
                    resolve(setHeroOut(docClient, heroID, requestedTimeOut, outOrNot));
                // if hero can't go for any reason, let them know
                } else {
                    log("hero can't go");
                    resolve("I'm sorry, <@"+heroID+">. There will be more than two heroes out at a time if you go now. If you really must run, check with your lead first.");
                }
            //if hero is scheduled to be out within a 15 minute window, let them go.
            } else if (heroList && heroList[heroID] && ((heroList[heroID].timeOut - currentTime <= 900)|| (currentTime - heroList[heroID].timeOut <= 900))) {
                if (bl.canHeroGo(heroList, requestedTimeOut, message)) {
                    // update the document with a new item
                    let heroDataValues = {
                        heroID: heroID,
                        timeOut: currentTime,
                        timeBack: 0,
                        isOut: true
                    }
                    db.updateItemDB(docClient, currentTime, heroDataValues, function(err){
                        if (err) {
                            resolve(handleErrors(err));
                        }
                        resolve("Go for it, <@"+heroID+">! Have a good lunch!");
                    });
                } else {
                    resolve("I'm sorry, <@"+heroID+">. There will be more than two heroes out at a time if you go now. Wait until your time! If you really must run, check with your lead first.");
                }

            // if the hero does not have an entry, check to see if they can go to lunch
            } else if (heroList && !heroList[heroID]) {

                if (bl.canHeroGo(heroList, requestedTimeOut, message)) {
                    // update the document with a new item
                    resolve(setHeroOut(docClient, heroID, requestedTimeOut, outOrNot));
                // if hero can't go for any reason, let them know
                } else {
                    resolve("I'm sorry, <@"+heroID+">. There will be more than two heroes out at a time if you go now. If you really must run, check with your lead first.");
                }
            } else {
                resolve(handleErrors(err));
            }
        })
    });
}

//deleted
function deleteMe(docClient, currentTime, heroID){
    // this has not been completed - I haven't quite decided how I want to handle this.
    // be sure to return a promise once you decide how to handle deletion
    return new Promise(function(resolve, reject) {
        db.getItemsDB(docClient, currentTime, function(err, heroList) {
            if (err) {
                return resolve(handleErrors(err));
            }
            if (heroList && heroList[heroID]) {
            // update the document with a new item
                let heroDataValues = {
                    heroID: heroID,
                    timeOut: 0,
                    timeBack: 0,
                    isOut: false
                }
                db.updateItemDB(docClient, currentTime, heroDataValues, function(err, data){
                    if (err){
                        resolve(handleErrors(err));
                    }
                    resolve("Your lunch entry has been deleted.");
                });
            } else {
                resolve("You don't have a lunch time reserved in my database. Schedule lunch in advance using `/supportlunch plz`?");
            }
        });
    });
}

function setHeroOut(docClient, heroID, requestedTime, outOrNot){
    let heroDataValues = {
        heroID: heroID,
        timeOut: requestedTime,
        timeBack: 0,
        isOut: outOrNot
    }
    return new Promise(function(resolve,reject) {
        if (outOrNot) {
            db.updateItemDB(docClient, requestedTime, heroDataValues, function(err){
                if (err) {
                    resolve(handleErrors(err));
                }
                resolve("Go for it, <@"+heroID+">! Have a good lunch!");
            });
        } else {
            db.updateItemDB(docClient, requestedTime, heroDataValues, function(err){
                if (err) {
                    resolve(handleErrors(err));
                }

                resolve("Okay <@"+heroID+">, I have you scheduled for "+ dataparser.formatTimestamp(requestedTime) + ".");
            });
        }
    })
}

function handleErrors(err){
    error(err);
    if (err && err.message){
        if (err.message == "ExpressionAttributeValues contains invalid value: A value provided cannot be converted into a number for key :object"){
            return "I wasn't able to understand what time you are trying to request for lunch. Please specify a time in the format HH:MM am/pm.";
        }
        return "There was a problem! Tell @Liz: " + err.message + ".";
    }
    return "There was a problem! Tell @Liz: " + err + ".";
}

function getMonthName(currentTime) {
    let readableMonth;
    switch (currentTime.getMonth()) {
        case "0":
            readableMonth = "Jan";
            break;
        case "1":
            readableMonth = "Feb";
            break;
        case "2":
            readableMonth = "Mar";
            break;
        case "3":
            readableMonth = "Apr";
            break;
        case "4":
            readableMonth = "May";
            break;
        case "5":
            readableMonth = "Jun";
            break;
        case "6":
            readableMonth = "Jul";
            break;
        case "7":
            readableMonth = "Aug";
            break;
        case "8":
            readableMonth = "Sep";
            break;
        case "9":
            readableMonth = "Oct";
            break;
        case "10":
            readableMonth = "Nov";
            break;
        case "11":
            readableMonth = "Dec";
            break;
    }
    return readableMonth;
}

module.exports = {
    getFullSchedule:getFullSchedule,
    backMeow:backMeow,
    survey:survey,
    invalidMessageTxt:invalidMessageTxt,
    whoIsOut:whoIsOut,
    wtfMate:wtfMate,
    motherMayI:motherMayI,
    deleteMe:deleteMe
};
