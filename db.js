const { getMSTDateString } = require('./stringtotimestamp');
let { log, error } = require('./utils/logger')('db');
// i need to:
// create document if it doesn't exist [empty template]
// update template with additional Items in the database
/*
https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/DynamoDB/DocumentClient.html#update-property
https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.ExpressionAttributeNames.html
*/

// creates document for the day
function createItemDB(docClient, currentTime, callback) {
    var todaysMSTDateString = getMSTDateString(currentTime);
    const updateDBParams = {
        TableName: "HeroLunch",
        Item: {
            date: todaysMSTDateString,
            heroes: {}
        },
        ConditionExpression: "attribute_not_exists(#date)",
        ExpressionAttributeNames: {
            "#date": "date"
        }
    };

    docClient.put(updateDBParams, function(err, data) {
        if (err) {
            // console.error("createItemDB unable to add item. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            // console.log("createItemDB added item:", JSON.stringify(data, null, 2));
            if (callback) {
                callback();
            };
        }
    });
}

function updateItemDB(docClient, currentTime, heroDataValues, callback) {
    var todaysMSTDateString = getMSTDateString(currentTime);
    const heroID = heroDataValues.heroID;
    //let updateDBParams = {};
    const updateDBParams = {
        TableName: "HeroLunch",
        Key: {
            date: todaysMSTDateString,
        },
        UpdateExpression: "SET #heroes.#heroID = :object",
        ExpressionAttributeNames: {
            "#heroes": "heroes",
            "#heroID": heroID
        },
        ExpressionAttributeValues: {
            ":object" : {
                timeOut: heroDataValues.timeOut,
                timeBack: heroDataValues.timeBack,
                isOut: heroDataValues.isOut
            }
        }
    };
    // console.log(updateDBParams);
    docClient.update(updateDBParams, function(err, data) {

        //if error, deal with it
        if (err && err.message === noItemError) {
            createItemDB(docClient, currentTime, function(){
                updateItemDB(docClient, currentTime, heroDataValues, callback);
            });
        } else if (callback) {
            // I'm not passing data here because it's not being used anywhere
            callback(err);
        }
    });
}


function getItemsDB(docClient, currentTime, callback) {
    var todaysMSTDateString = getMSTDateString(currentTime);
    //log("db");
    const updateDBParams = {
        TableName : "HeroLunch",
        Key: {
            date: todaysMSTDateString,
        },
        ProjectionExpression: "#heroesNames",
        ExpressionAttributeNames: {
            "#heroesNames": "heroes"
        },
    };
    docClient.get(updateDBParams, function(err, data) {
        let cleanedData = {};
        if (data && data.Item) {
            cleanedData = data.Item.heroes;
        }
        //if error, deal with it
        if (err && err.message === noItemError) {
            log("no item error");
            createItemDB(docClient, currentTime, function(){
                callback && callback(null, cleanedData);
            });
        } else if (callback) {
            if (err && err.message){
                log("other error:"+err.message);
                // ensure that if err, cleanedData doesn't get sent to the callback
                return callback(err, null);
            }
            // ensure that err doesn't get sent to the callback if there isn't a real error
            callback(null, cleanedData);
        }
    });
}

const noItemError = "The document path provided in the update expression is invalid for update";


module.exports = {
    createItemDB:createItemDB,
    updateItemDB:updateItemDB,
    getItemsDB:getItemsDB
};
