const {
    parseGetOutData,
    parseGetAllData,
} = require("./dataparser");

const {
    referenceTimestamp,
    minutes,
} = require('./test-fixtures/reference-time');

const exampleData1 = {
    // "Item": {
        // "heroes": {
            "nooover": { timeOut: referenceTimestamp, isOut: true, timeBack: 0 },
        // }
    // }
};
const exampleData2 = {
    // "Item": {
        // "heroes": {
            "naat": { timeOut: referenceTimestamp - minutes(35), isOut: false, timeBack: referenceTimestamp - minutes(1) },
            "Leez": { timeOut: referenceTimestamp - minutes(28), isOut: true, timeBack: 0 },
        // }
    // }
};
const exampleData3 = {
    // "Item": {
        // "heroes": {
            "Leez": { timeOut: referenceTimestamp - minutes(2), isOut: true, timeBack: 0 },
            "noover": { timeOut: referenceTimestamp - minutes(2), isOut: true, timeBack: 0 },
        // }
    // }
};

describe('parseGetOutData', () => {

    it('returns a string', () => {
        const results = parseGetOutData(exampleData1);

        // Results should be a string
        expect(
            typeof results
        ).toEqual(
            'string'
        );
    });


    //something wacky is happening here. if i change the expected string, it works.
    //as in, the received string is the correct one, sans the addition.
    it('formats usernames and timestamps with Slack formatting', () => {
        const { timeOut } = exampleData1.nooover;
        const timeBack = timeOut + minutes(30);
        const currentTime = timeOut + minutes(10);
        expect(
            parseGetOutData(exampleData1, currentTime)
        ).toEqual(
            `${formatUsername('nooover')} went for lunch at ${formatTimestamp(timeOut)} and should be back at ${formatTimestamp(timeBack)}.`
        );
    });

    it('returns only heroes who are currently out', () => {
        const { timeOut } = exampleData2.Leez;
        const timeBack = timeOut + minutes(30);
        expect(
            parseGetOutData(exampleData2)
        ).toEqual(
            // naat is not included because they are not currently out
            `${formatUsername('Leez')} went for lunch at ${formatTimestamp(timeOut)} and should be back at ${formatTimestamp(timeBack)}.`
        );
    });

    it('will return multiple heroes who are out with a linebreak in between', () => {
        const { timeOut: timeOut1 } = exampleData3.Leez;
        const { timeOut: timeOut2 } = exampleData3.noover;
        const timeBack1 = timeOut1 + minutes(30);
        const timeBack2 = timeOut2 + minutes(30);
        expect(
            parseGetOutData(exampleData3)
        ).toEqual(
            `${formatUsername('Leez')} went for lunch at ${formatTimestamp(timeOut1)} and should be back at ${formatTimestamp(timeBack1)}.\n` +
            `${formatUsername('noover')} went for lunch at ${formatTimestamp(timeOut2)} and should be back at ${formatTimestamp(timeBack2)}.`
        );
    });

});

describe('parseGetAllData', () => {

    it('returns a string', () => {
        const { timeOut } = exampleData1.nooover;
        //I added a currentTime
        const currentTime = timeOut + minutes(10);
        const timeBack = timeOut + minutes(30);
        expect(
            parseGetAllData(exampleData1, currentTime)
        ).toEqual(
            `${formatUsername('nooover')} went on lunch at ${formatTimestamp(timeOut)} and should be back at ${formatTimestamp(timeBack)}.`
        );
    });

    it('returns all heroes, whether currently out or not', () => {
        const { timeOut:timeOut1 } = exampleData2.Leez;
        const { timeOut:timeOut2, timeBack:timeBack2 } = exampleData2.naat; // use actual time back, since it is available
        const timeBack1 = timeOut1 + minutes(30);
        const currentTime = timeOut1 + minutes(5);
        expect(
            parseGetAllData(exampleData2, currentTime)
        ).toEqual(
            `${formatUsername('naat')} scheduled lunch from ${formatTimestamp(timeOut2)} to ${formatTimestamp(timeBack2)}.\n` +
            `${formatUsername('Leez')} went on lunch at ${formatTimestamp(timeOut1)} and should be back at ${formatTimestamp(timeBack1)}.`
        );
    });

});

function formatUsername(username) {
    return "<@" + username + ">";
}

function formatTimestamp(timestamp, fallback = "cannot_display_time") {
    if (fallback) fallback = "|" + fallback;
    return "<!date^" + timestamp + "^{time}" + fallback + ">";
}
