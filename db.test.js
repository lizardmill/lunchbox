const {
    createItemDB,
    updateItemDB,
    getItemsDB,
} = require('./db');

const getDynamoDBClientMock = require('./test-fixtures/dynamodb');
const { info } = require('./utils/logger')('db.test');
const { referenceTimestamp } = require('./test-fixtures/reference-time')

// An example object matching the schema expected of database items (kind of)
const exampleData = {
    Item: {
        date: 'meow',
        heroes: {
            heroID1: {
                someData: "meow"
            },
            heroID2: {}
        }
    }
};
// Some error
const exampleError = new Error("Error on line \"+CHICKEN+\": expected 'chicken'");
// Error returned by DynamoDB when a document path in an update expression does not exist
const invalidDocumentPathError = new Error("The document path provided in the update expression is invalid for update");

describe('createItemDB', () => {

    it('will put a new item', () => {
        const mockClient = getDynamoDBClientMock();

        createItemDB(mockClient, referenceTimestamp);

        // Should have called put only once
        expect(mockClient.put).toBeCalled();
        expect(mockClient.put.mock.calls.length).toEqual(1);
        ['get', 'update'].map(method =>
            expect(mockClient[method]).not.toBeCalled()
        );

        // docClient expects a callback to be provided
        expect(typeof mockClient.getCb('put')).toEqual('function');

        // MOCK CLIENT: Invoke put callback function with (mocked) results of put call
        mockClient.invokePutCb(null, exampleData);
    });

    it('accepts a success callback, will get called if put is successful', () => {
        const mockClient = getDynamoDBClientMock();
        const successCallback = jest.fn();

        createItemDB(mockClient, referenceTimestamp, successCallback);

        // MOCK CLIENT: Return success from docClient 'put' call
        mockClient.invokePutCb(null, exampleData);

        // Our successCallback passed to createItemDB should get called once
        expect(successCallback).toBeCalled();
        expect(successCallback.mock.calls.length).toEqual(1);
    });

    it('will not invoke callback if put is unsuccessful', () => {
        const mockClient = getDynamoDBClientMock();
        const successCallback = jest.fn();

        createItemDB(mockClient, referenceTimestamp, successCallback);

        // MOCK CLIENT: Return an error from db client put call
        mockClient.invokePutCb(exampleError, null);

        // Our successCallback passed to createItemDB should not get called
        expect(successCallback).not.toBeCalled();
    });

});

describe('updateItemDB', () => {

    it('will update the current item', () => {
        const mockClient = getDynamoDBClientMock();
        const successCallback = jest.fn();

        const heroData = {
            isOut: false,
            timeOut: 1522167770,
            heroID: 'example-ID'
        }

        updateItemDB(mockClient, referenceTimestamp, heroData);

        // Should have called client update
        expect(mockClient.update).toBeCalled();

        // MOCK CLIENT: Return success from db client update call
        mockClient.invokeUpdateCb(null, exampleData);

        // Should not have made any further calls to client
        expect(mockClient.update.mock.calls.length).toEqual(1);
        ['put', 'get'].map(method =>
            expect(mockClient[method]).not.toBeCalled()
        );
    });


    it('will create a new item if it does not already exist, and then apply the update', () => {
        const mockClient = getDynamoDBClientMock();
        const successCallback = jest.fn();

        const heroData = {
            isOut: false,
            timeOut: 1522167770,
            heroID: 'example-ID'
        }

        updateItemDB(mockClient, referenceTimestamp, heroData);

        // Should call client update once
        expect(mockClient.update).toBeCalled();
        expect(mockClient.update.mock.calls.length).toEqual(1);

        // MOCK CLIENT: Return 'invalid document path' error from db 'update' call
        mockClient.invokeUpdateCb(invalidDocumentPathError, null);

        // Should call client put once, to create the document
        expect(mockClient.put).toBeCalled();
        expect(mockClient.put.mock.calls.length).toEqual(1);

        // MOCK CLIENT: Return success from db client put call
        mockClient.invokePutCb(null, exampleData);

        // Should call client update again, to finish the original task now that the document exists
        expect(mockClient.update.mock.calls.length).toEqual(2);

        // MOCK CLIENT: Return success from second db client update call
        mockClient.invokeUpdateCb(null, exampleData);
    });

});

describe('getItemsDB', () => {

    it('does things without blowing up', () => {
        const mockClient = getDynamoDBClientMock();

        getItemsDB(mockClient, referenceTimestamp);

        // MOCK CLIENT: Return success from db get call
        mockClient.invokeGetCb(null, exampleData);
    });

    it('accepts a callback, that will get invoked with null (argument 1) and data (argument 2), if no error occured', () => {
        const mockClient = getDynamoDBClientMock();
        const ourCallback = jest.fn();

        getItemsDB(mockClient, referenceTimestamp, ourCallback);

        // MOCK CLIENT: Return success from db get call
        mockClient.invokeGetCb(null, exampleData);

        // Our callback should have been called once
        expect(ourCallback).toBeCalled();
        expect(ourCallback.mock.calls.length).toEqual(1);

        // Our callback should be provided with arguments (null, data)
        const firstOurCallbackArgs = ourCallback.mock.calls[0];
        expect(firstOurCallbackArgs[0]).toBeNull();
        expect(firstOurCallbackArgs[1]).toEqual(exampleData.Item.heroes);

    });

    it('accepts a callback, that will get invoked with an error (argument 1) and null (argument 2), if an error occured', () => {
        const mockClient = getDynamoDBClientMock();
        const ourCallback = jest.fn();

        getItemsDB(mockClient, referenceTimestamp, ourCallback);

        // MOCK CLIENT: Return error from db 'get' call
        mockClient.invokeGetCb(exampleError, null);

        // Our callback should have been called once
        expect(ourCallback).toBeCalled();
        expect(ourCallback.mock.calls.length).toEqual(1);

        // Our callback should be provided with arguments (error, null)
        const argsProvidedToOurCb = ourCallback.mock.calls[0];
        expect(argsProvidedToOurCb[0]).toEqual(exampleError);
        expect(argsProvidedToOurCb[1]).toBeNull();
    });

});
