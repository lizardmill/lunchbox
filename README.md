# Lunchbox

A friendly neighborhood chatbot that just wants to help you go to lunch.

## Testing

Lunchbox has both unit and integration tests. To run unit tests:

    yarn test

Integration tests use [LocalStack](https://github.com/localstack/localstack) to run a local DynamoDB service. This will need to be installed on your system to run integration tests (`pipenv install`).

To run integration tests, first start the local service and seed tables:

    yarn local-start &
    yarn local-seed

With this done, you are ready to run integration tests:

    yarn test-integration

Hint: don't forget to stop the local service, if running in the background.

    jobs
    kill %

## Deploying new environment

To create:

    claudia create --region us-west-2 --api-module lambda --config claudia-ENV-NAME.json --name lunchbox-ENV-NAME

To update/re-deploy:

    claudia update --config claudia-ENV-NAME.json

Follow the instructions to set up an App with Slash Commands: https://api.slack.com/slash-commands
Once installed to a workplace, configure token:
    claudia update --configure-slack-slash-command --config claudia-ENV-NAME.json

When asked for a "outgoing webhook token", just use the slack token again (?).

IAM role will also need to updated to allow access to DynamoDB (attach inline policy, easiest to C+P JSON from existing role).

Note that for now, this is using the same database table ("lunchbox") regardless of environment. It would be ideal to autodetect which env is being used, and use table "lunchbox-ENV-NAME" (automatically creating table if necessary).

## Misc Tips

If you get just an empty object `{}` as a response to a slash command, it may
be that the script died. Check lambda logs.
