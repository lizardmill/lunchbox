module.exports = () => {
  // https://facebook.github.io/jest/docs/en/mock-functions.html
  function MockClient() {
    this.put = jest.fn();
    this.get = jest.fn();
    this.update = jest.fn();
  }

  MockClient.prototype.getCb = function (method, call = 0) {
    if (this[method].mock.calls.length < call + 1) {
      throw new Error(`Mock "${method}" method has been called ${this[method].mock.calls.length} time(s), should have been called at least ${call + 1} time(s).`);
    }
    if (typeof this[method].mock.calls[call][1] !== 'function') {
      throw new Error(`Second argument provided to mock "${method}" method is not a function.`);
    }
    return this[method].mock.calls[call][1];
  }

    MockClient.prototype.getParamObject = function (method, call = 0) {
    if (this[method].mock.calls.length < call + 1) {
      throw new Error(`Mock "${method}" method has been called ${this[method].mock.calls.length} time(s), should have been called at least ${call + 1} time(s).`);
    }
    if (typeof this[method].mock.calls[call][0] !== 'object') {
      throw new Error(`Second argument provided to mock "${method}" method is not an object.`);
    }
    return this[method].mock.calls[call][0];
  }

  MockClient.prototype.invokeCb = function(method, error, data) {
    // Keep track of how many times callback has been invoked, so each only gets called one time
    this._cbInvokes = this._cbInvokes || {};
    const call = this._cbInvokes[method] || 0;
    this._cbInvokes[method] = call + 1;
    return this.getCb(method, call)(error, data);
  }

  MockClient.prototype.invokeGetCb = function(error, data) {
    return this.invokeCb('get', error, data);
  }

  MockClient.prototype.invokePutCb = function(error, data) {
    return this.invokeCb('put', error, data);
  }

  MockClient.prototype.invokeUpdateCb = function(error, data) {
    return this.invokeCb('update', error, data);
  }

  return new MockClient();
};