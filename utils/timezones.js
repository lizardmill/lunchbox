// https://stackoverflow.com/a/11888430
const stdTimezoneOffset = function (date=new Date()) {
    var jan = new Date(date.getFullYear(), 0, 1);
    var jul = new Date(date.getFullYear(), 6, 1);
    return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
}
const isDstObserved = function (date=new Date()) {
    // console.log('actual', date.getTimezoneOffset(), 'standard', stdTimezoneOffset(date));
    return date.getTimezoneOffset() < stdTimezoneOffset(date);
}
// MDT in summer, use +6. MST in winter, use +7
const MST_OFFSET_HRS = isDstObserved() ? 6 : 7;

const getHoursMST = (time) => {
    // Get the current hour of day, in MST
    let hour = time.getUTCHours() - MST_OFFSET_HRS;
    // If UTC hour is less than offset, roll in to previous day (eg UTC = 3, MST = 21)
    if (hour < 0) hour += 24;
    return hour;
};

module.exports = {
    MST_OFFSET_HRS: MST_OFFSET_HRS,
    getHoursMST: getHoursMST,
};