const botBuilder = require("claudia-bot-builder");
const AWS = require("aws-sdk");
const db = require("./db.js");
const bl = require("./businesslogic.js");
const dataparser = require("./dataparser.js");
const ctrl = require("./controller.js");
const { info, log, error } = require('./utils/logger')('stringtotimestamp');

let awsConfig = { region: 'us-west-2' };
if (process.env.DYNAMODB_URL) {
    awsConfig.endpoint = process.env.DYNAMODB_URL
}
AWS.config.update(awsConfig);

const docClient = new AWS.DynamoDB.DocumentClient();

module.exports = botBuilder(function(message) {
    // set the ID of the querying hero (sender)
    const heroID = message.sender;
    // set message text + ensure it's all lowercase
    const messageText = message.text.toLowerCase();
    const currentTime = Math.floor(Date.now()/1000);

    if (messageText === "what") {
        return ctrl.wtfMate(heroID);
    } else if (messageText === "who") {
        return ctrl.whoIsOut(docClient);
    } else if (messageText === "all") {
        return ctrl.getFullSchedule(docClient, currentTime);
    } else if (messageText === "now") {
        return ctrl.motherMayI(docClient, currentTime, heroID, messageText);
    } else if (messageText === "bek" || messageText === "back") {
        return ctrl.backMeow(docClient, currentTime, heroID);
    } else if (messageText.startsWith("plz")) {
        log("plz running");
        return ctrl.motherMayI(docClient, currentTime, heroID, messageText);
    } else if (messageText === "delete") {
        return ctrl.deleteMe(docClient, currentTime, heroID);
    } else if (messageText === "feedback") {
        return ctrl.survey();
    } else {
        return ctrl.invalidMessageTxt(messageText);
    }
}, { platforms: ["slackSlashCommand"] });
