const { log, error } = require('./utils/logger')('businesslogic');

function canHeroGo(heroList, requestedLunchTime, message) {
    let heroesInRequestedWindow = [];
    const estimatedLunchBack = requestedLunchTime + 1800;

    if (!heroList) {
        return true;
    }
    log(heroList);
    for (let hero in heroList) {

        const scheduledHeroTimeOut = heroList[hero].timeOut;
        const scheduledHeroTimeBack = heroList[hero].timeOut+1800;

        if (
            (scheduledHeroTimeBack > requestedLunchTime && scheduledHeroTimeBack < estimatedLunchBack) ||
            (scheduledHeroTimeOut > requestedLunchTime && scheduledHeroTimeOut < estimatedLunchBack)
        ) {
            //add to list of scheduled heroes
            heroesInRequestedWindow.push(heroList[hero]);
        }
    }
    log(heroesInRequestedWindow);
    // if three or more heroes are out, the answer is just no
    if (heroesInRequestedWindow.length >= 3) {
        return false;   //CALLBACK
    }
    // check if there is overlap
    if (heroesInRequestedWindow.length === 2) {
        if (isOverlap(heroesInRequestedWindow[0], heroesInRequestedWindow[1])) {
            return false;
        }
    }
    // if the length is zero, one, or two with no overlap

    // check if AWOL heroes are preventing lunch from people trying to schedule immediately
    // if (message === "now" ) {
    return canHeroGoIfAWOLHeroesExist(heroList, requestedLunchTime, heroesInRequestedWindow);
    // }

    // go for lunch.
    return true;
};

const isOverlap = (heroA, heroB) => {
    if (heroA.timeOut === heroB.timeOut) {
        return true;
    }
    if (heroA.timeOut < heroB.timeOut) {
        return !(heroA.timeOut+1800 < heroB.timeOut);
    }
    return isOverlap(heroB, heroA);
}

const getAWOLHeroes = (heroList, currentTime) => {
    let listAWOLHeroes = [];
    for (let hero in heroList) {
        const scheduledHeroTimeBack = heroList[hero].timeOut+1800;
        if (scheduledHeroTimeBack < currentTime && heroList[hero].isOut) {
            //then hero is AWOL
            listAWOLHeroes.push(heroList[hero]);
        }
    }
    return listAWOLHeroes;
}

const canHeroGoIfAWOLHeroesExist = (heroList, requestedLunchTime, listOfHeroes) => {
    //if currentlyoutheroes are AWOL, decide if it matters
    const currentAWOLHeroes = getAWOLHeroes(heroList, requestedLunchTime);
    // no awol heroes to prevent lunch
    if (currentAWOLHeroes.length < 1) {
        return true;
    }
    // how many are in the list of heroes?
    // is one awol hero going to prevent lunch?
    if (currentAWOLHeroes.length === 1) {
        if (listOfHeroes.length === 0 ) {
            return true;
        }
        if (listOfHeroes.length === 1) {
            // if one person is AWOL and one scheduled,
            // return false if scheduled is currently out.
            return !listOfHeroes[0].isOut;
        }
        // we may not need this case. but leaving for future testing
        // if (listOfHeroes.length === 2) {
        //     // neither can overlap
        //     return (
        //         !isOverlap(listOfHeroes[0], currentAWOLHeroes[0]) &&
        //         !isOverlap(listOfHeroes[1], currentAWOLHeroes[0])
        //     );
        // }
        // this should never happen (listOfHeroes >= 2)
        return false;
    }
    // two many awol heroes auto prevent lunch
    if (currentAWOLHeroes.length >= 2) {
        return false;
    }
}


module.exports = {
    canHeroGo:canHeroGo,
    // just for testing
    _isOverlap:isOverlap,
    _getAWOLHeroes:getAWOLHeroes
};
