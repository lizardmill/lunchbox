const { log } = require('./utils/logger')('dataparser');

function parseGetOutData(heroList) {
    let currentAtLunchList = [];
    for (let entry in heroList) {
        if (heroList[entry].isOut) {
            const heroTimeOut = heroList[entry].timeOut;
            const heroTimeBack = heroList[entry].timeOut+1800;
            currentAtLunchList.push("<@"+entry+"> went for lunch at "+ formatTimestamp(heroTimeOut) + " and should be back at "+formatTimestamp(heroTimeBack)+".");
        }
    }
    if (currentAtLunchList.length === 0) {
        return "No one is currently at lunch."
    }
    return currentAtLunchList.join("\n");
}

function parseGetAllData(heroList, currentTime) {
    let dailyLunchList = [];
    for (let entry in heroList) {
        let currentTimeOut = heroList[entry].timeOut;
        let currentTimeBack = heroList[entry].timeBack;
        let currentIsOut = heroList[entry].isOut;
        log(heroList[entry], currentTimeOut, currentTimeBack, currentIsOut);
        log(currentTime);
        // if timeOut is 0, then the original entry has been "deleted"
        if (currentTimeOut === 0) {
            continue;
        }

        //if they not back from lunch
        if (!currentTimeBack) {
            currentTimeBack = currentTimeOut + 1800;
            //if they are currently out
            if (currentTimeOut <= currentTime && currentIsOut) {
                dailyLunchList.push("<@"+entry+"> went on lunch at "+formatTimestamp(currentTimeOut)+" and should be back at "+formatTimestamp(currentTimeOut+1800)+".");
            } else if (currentTimeOut <= currentTime && !currentIsOut) {
                //if they are supposed to be out, but have not marked themselves as out
                dailyLunchList.push("<@"+entry+"> scheduled lunch at "+formatTimestamp(currentTimeOut)+", but has not marked themselves away.");
            } else {
                //if they are scheduled in the future
                dailyLunchList.push("<@"+entry+"> has lunch scheduled at "+formatTimestamp(currentTimeOut)+".");
            }
        } else {
            dailyLunchList.push("<@"+entry+"> scheduled lunch from "+formatTimestamp(currentTimeOut)+" to " + formatTimestamp(currentTimeBack)+".");
        }
    }
    // if dailylunchlist is empty, return a message that makes sense.
    log(dailyLunchList, heroList);
    if (dailyLunchList.length < 1) {
        return "No one has scheduled or is currently at lunch.";
    }
    return dailyLunchList.join("\n");
}

function formatTimestamp(timestamp, fallback = "cannot_display_time") {
    if (fallback) fallback = "|" + fallback;
    return "<!date^" + timestamp + "^{time}" + fallback + ">";
}

module.exports = {
    parseGetOutData:parseGetOutData,
    parseGetAllData:parseGetAllData,
    formatTimestamp:formatTimestamp
}
