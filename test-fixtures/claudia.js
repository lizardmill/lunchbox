module.exports = function claudiaMock (lambda) {
    const contextSpy = {
        done: jest.fn(),
    };

    let getResponseIndex = 0;
    const sendCommandAndGetResponse = (message, user) => simulateSlackCommand(lambda, contextSpy, message, user)
            .then(() => {
                const response = getResponse(contextSpy, getResponseIndex);
                getResponseIndex += 1;
                return response;
            });
    return {
        simulateSlackCommand: sendCommandAndGetResponse
    };
};

const getResponse = (contextSpy, responseIndex) => {
    const rawbody = getResponseRaw(contextSpy, responseIndex);
    try {
        return JSON.parse(rawbody);
    } catch (err) {
        console.error(err);
        throw new Error(`Error while JSON parsing Claudia bot response object.`)
    }
}

const getResponseRaw = (contextSpy, responseIndex) => {
    if (contextSpy.done.mock.calls.length < responseIndex + 1) {
      throw new Error(`Claudia bot 'done' callback was called ${contextSpy.done.mock.calls.length} time(s), should have been called at least ${responseIndex + 1} time(s).`);
    }
    try {
        return contextSpy.done.mock.calls[responseIndex][1]['body'];
    } catch (err) {
        console.error(err);
        throw new Error(`Claudia bot did not supply expected response object in 'done' callback.`)
    }
}

const simulateSlackCommand = (lambda, contextSpy, message, userid) => lambda.proxyRouter({
    requestContext: {
        resourcePath: '/slack/slash-command',
        httpMethod: 'POST'
    },
    headers: {
        'content-type': 'application/x-www-form-urlencoded'
    },
    body: `token=MEOW&command=/lunch&text=${message}&user_id=${userid}`,
    stageVariables: {
        slackVerificationToken: 'MEOW'
    }
}, contextSpy)