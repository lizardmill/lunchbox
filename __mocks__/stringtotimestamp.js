const {
    referenceTimestamp,
    minutes,
} = require('../test-fixtures/reference-time');

module.exports = {
    convert: (timeString) => {
        switch (timeString) {
            case '10:30': return referenceTimestamp;
            case '11:00': return referenceTimestamp + minutes(30);
            case '11:30': return referenceTimestamp + minutes(60);
            case '12:00': return referenceTimestamp + minutes(90);
            case '12:30': return referenceTimestamp + minutes(120);
            case '1:00': return referenceTimestamp + minutes(150);
            case 'cats': return NaN;
            default: throw new Error(`stringtotimestamp mock not implemented for time "${timeString}"`);
        }
    },
    getMSTDateString: (timestamp) => {
        // console.log('getMSTDateString', timestamp);
        // referenceTimestamp is: Sun Jun 10 10:30:00 MDT 2018
        return '6/10/2018';
    }
};