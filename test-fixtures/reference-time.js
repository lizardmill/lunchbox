// Sun Jun 10 10:30:00 MDT 2018
const referenceTimestamp = 1528648200;
// Convert minutes to seconds, for ex to be summed with a timestamp
const minutes = (n) => n * 60;

module.exports = {
    referenceTimestamp: referenceTimestamp,
    minutes: minutes,
};