const { info, log, error } = require('./utils/logger')('stringtotimestamp');
const { MST_OFFSET_HRS } = require('./utils/timezones');
const spacetime = require('spacetime');

/*
 * converts a string like '12:00pm' in to a timestamp, assuming the time is in MST
 * and refers to today. `now` parameter is for unit tests, defaults to right meow.
 * possible time string examples 2, 12, 2:14, 12:45, 2pm, 2:24 pm, 14:30, 9:00am, 9am
 * FOR FUTURE REFERENCE:
 * If i want to allow for user specifying different timezones, signature could maybe be:
 * convert(string, tz = 'America/Denver', now = Date.now())
 * Eg, accept timezone + timestamp, then build the spacetime object inside of the function
 * to allow for testable code (`now` would only be provided by caller during testing, `tz` would be optional).
 */
function convert(string, requestedTime = spacetime.now('America/Denver')) {
    log("convert running");
    let timeNumbers = getTimeNumbersFromString(string);
    if (!timeNumbers || timeNumbers.length !== 2) {
        return NaN;
    }
    let timeHour = timeNumbers[0];
    let timeMinute = timeNumbers[1];

    requestedTime.hour(timeHour);
    requestedTime.minutes(timeMinute);
    requestedTime.seconds(0);

    return Math.floor(requestedTime.epoch/1000);
}

function getTimeNumbersFromString(strang) {
    let timeHour;
    let timeMinute;
    //STRANG CLEANING
    if (typeof strang != "string" || strang.length < 1){
        //log("DQed");
        return NaN;
    }
    strang = strang.toLowerCase();

    const isPM = strang.includes('p');
    strang = strang.replace(/\s?[a,p]m?/, '');

    if (strang.length > 5) {
        return NaN;
    }
    if (strang.length <= 2) {
        timeHour = Number(strang);
        timeMinute = 0;
        if (timeHour > 24) {
            return NaN;
        }
    }
    if (strang.includes(':')) {
        let arr = strang.split(':');
        timeHour = Number(arr[0]);
        if (timeHour > 24){
            return NaN;
        }
        timeMinute = Number(arr[1]);
        if (timeMinute > 60){
            return NaN;
        }
    }

    if (!timeHour) {
        return NaN;
    }

    if (isPM && timeHour < 12) {
        timeHour += 12;
    }
    return [timeHour, timeMinute];
}

function getMSTDateString(currentTime) {
    let today = new Date((currentTime * 1000));
    today.setUTCHours(today.getUTCHours() - MST_OFFSET_HRS);
    var _todaysDate = today.getUTCMonth()+1 +"/"+ today.getUTCDate() +"/"+today.getUTCFullYear();
    return _todaysDate;
}

module.exports = {
    convert: convert,
    getMSTDateString: getMSTDateString
}
