const AWS = require("aws-sdk");
const lambda = require('./lambda');
const claudiaMock = require('./test-fixtures/claudia');
const { getHoursMST } = require('./utils/timezones');
const { info, log, error } = require('./utils/logger')('lambda.test-integration');
AWS.config.update({
  region: "us-west-2",
  endpoint: "http://localhost:4569"
});
const docClient = new AWS.DynamoDB.DocumentClient();

const verifyIsEarlyEnoughToSchedule = () => {
    // Verifying that the current time of day (MST) is before 11pm
    // This way, we know we can "schedule in advance" for 11pm
    if (getHoursMST(new Date()) < 23) {
        return true;
    }

    error('Current hour MST is too late to do any scheduling. Skipping test.');
    return false;
}

describe('Lambda', () => {
    let claudia;

    beforeEach((done) => {
        claudia = claudiaMock(lambda);

        // Delete all items from table in between tests
        const TableName = 'HeroLunch';
        docClient.scan({TableName}, (err, data) => {
            if (data && data.Items) {
                info(`Found ${data.Items.length} items in db.`)
                return Promise.all(
                    data.Items.map(({date}) => new Promise((yay, nope) => {
                        info(`Deleting document with date ${date} from db.`);
                        docClient.delete({TableName, Key:{date}}, (err, data) => {
                            if (err) return nope(err);
                            yay(data);
                        });
                    }))
                )
                .then(() => done())
                .catch(err => {
                    error(err);
                    done.fail();
                });
            }
            done();
        });
    });

    it('meows if you use `what` subcommand', () => {
        return claudia
            .simulateSlackCommand('what', 'EA333')
            .then(response => {
                // Expect reply
                expect(response).toEqual({
                    text: "Hi <@EA333>. Lunchbox is a SG Support lunch bot. Request a lunch spot in advance by asking nicely (like `/supportlunch plz 1:30pm`) or see if you can just go now with `/supportlunch plz`. When you're done with lunch,remember to mark yourself as `/supportlunch bek` or you'll ruin everything. See who's out now with `/supportlunch who` and see everyone who's scheduled or gone to lunch so far today with `/supportlunch all`."
                });
            });
    });

    it('allows you to go to lunch with `now` subcommand', () => {
        return claudia
            .simulateSlackCommand('now', 'EA333')
            .then(response => {
                // Expect reply OK
                expect(response).toEqual({
                    text: "Go for it, <@EA333>! Have a good lunch!"
                });
            });
    });

    it('does not allow you to mark yourself out twice', () => {
        return claudia
            .simulateSlackCommand('now', 'EA333')
            .then(response => {
                // Expect reply OK
                expect(response).toEqual({
                    text: "Go for it, <@EA333>! Have a good lunch!"
                });
            })
            .then(() => claudia.simulateSlackCommand('now', 'EA333'))
            .then(response => {
                // Expect reply NO
                expect(response).toEqual({
                    text: "You are already marked away, yo."
                })
            });
    });

    it('allows you to schedule in advance', () => {
        if (!verifyIsEarlyEnoughToSchedule()) return;

        return claudia
            .simulateSlackCommand('all', 'EA333')
            .then(response => {
                // Expect empty list
                expect(response).toEqual({
                    text: "No one has scheduled or is currently at lunch."
                });
            })
            .then(() => claudia.simulateSlackCommand('plz 11:00pm', 'EA333'))
            .then(response => {
                // Expect reply OK
                expect(response).toEqual({
                    text: expect.stringContaining("Okay <@EA333>, I have you scheduled for <!date^1")
                });
            })
            .then(() => claudia.simulateSlackCommand('all', 'EA333'))
            .then(response => {
                // Expect user is in list
                expect(response).toEqual({
                    text: expect.stringContaining("<@EA333> has lunch scheduled at <!date^1")
                });
            });
    });

    it('allows you to delete your lunch', () => {
        if (!verifyIsEarlyEnoughToSchedule()) return;

        return claudia
            .simulateSlackCommand('plz 11:00pm', 'EA333')
            .then(response => {
                // Expect reply OK
                expect(response).toEqual({
                    text: expect.stringContaining("Okay <@EA333>, I have you scheduled for <!date^1")
                });
            })
            .then(() => claudia.simulateSlackCommand('all', 'EA333'))
            .then(response => {
                // Expect user is in list
                expect(response).toEqual({
                    text: expect.stringContaining("<@EA333> has lunch scheduled at <!date^1")
                });
            })
            .then(() => claudia.simulateSlackCommand('delete', 'EA333'))
            .then(response => {
                // Expect reply OK
                expect(response).toEqual({
                    text: "Your lunch entry has been deleted."
                });
            })
            .then(() => claudia.simulateSlackCommand('all', 'EA333'))
            .then(response => {
                // Expect empty list
                expect(response).toEqual({
                    text: "No one has scheduled or is currently at lunch."
                });
            });
    });

    it('allows blocky scheduling', () => {
        if (!verifyIsEarlyEnoughToSchedule()) return;

        return claudia
            .simulateSlackCommand('plz 11:00 pm', 'EA333')
            .then(response => {
                // Expect reply OK
                expect(response).toEqual({
                    text: expect.stringContaining("Okay <@EA333>, I have you scheduled for <!date^1")
                });
            })
            .then(() => claudia.simulateSlackCommand('plz 11p', 'NK422'))
            .then(response => {
                // Expect reply OK
                expect(response).toEqual({
                    text: expect.stringContaining("Okay <@NK422>, I have you scheduled for <!date^1")
                });
            })
            .then(() => claudia.simulateSlackCommand('plz 11:30pm', 'TT222'))
            .then(response => {
                // Expect reply OK
                expect(response).toEqual({
                    text: expect.stringContaining("Okay <@TT222>, I have you scheduled for <!date^1")
                });
            })
            .then(() => claudia.simulateSlackCommand('plz 11:30pm', 'TA211'))
            .then(response => {
                // Expect reply OK
                expect(response).toEqual({
                    text: expect.stringContaining("Okay <@TA211>, I have you scheduled for <!date^1")
                });
            });
    });

    it('allows blocky scheduling', () => {
        if (!verifyIsEarlyEnoughToSchedule()) return;

        return claudia
            .simulateSlackCommand('plz 11:30 pm', 'EA333')
            .then(response => {
                // Expect reply OK
                expect(response).toEqual({
                    text: expect.stringContaining("Okay <@EA333>, I have you scheduled for <!date^1")
                });
            })
            .then(() => claudia.simulateSlackCommand('plz 11:30p', 'NK422'))
            .then(response => {
                // Expect reply OK
                expect(response).toEqual({
                    text: expect.stringContaining("Okay <@NK422>, I have you scheduled for <!date^1")
                });
            })
            .then(() => claudia.simulateSlackCommand('plz 11pm', 'TT222'))
            .then(response => {
                // Expect reply OK
                expect(response).toEqual({
                    text: expect.stringContaining("Okay <@TT222>, I have you scheduled for <!date^1")
                });
            })
            .then(() => claudia.simulateSlackCommand('plz 11:00pm', 'TA211'))
            .then(response => {
                // Expect reply OK
                expect(response).toEqual({
                    text: expect.stringContaining("Okay <@TA211>, I have you scheduled for <!date^1")
                });
            });
    });

    it('allows you to (re)schedule after deleting', () => {
        if (!verifyIsEarlyEnoughToSchedule()) return;

        return claudia
            .simulateSlackCommand('plz 11:00pm', 'EA333')
            .then(response => {
                // Expect reply OK
                expect(response).toEqual({
                    text: expect.stringContaining("Okay <@EA333>, I have you scheduled for <!date^1")
                });
            })
            .then(() => claudia.simulateSlackCommand('all', 'EA333'))
            .then(response => {
                // Expect user is in list
                expect(response).toEqual({
                    text: expect.stringContaining("<@EA333> has lunch scheduled at <!date^1")
                });
            })
            .then(() => claudia.simulateSlackCommand('delete', 'EA333'))
            .then(response => {
                // Expect reply OK
                expect(response).toEqual({
                    text: "Your lunch entry has been deleted."
                });
            })
            .then(() => claudia.simulateSlackCommand('plz 11:15pm', 'EA333'))
            .then(response => {
                // Expect empty list
                expect(response).toEqual({
                    text: expect.stringContaining("Okay <@EA333>, I have you scheduled for <!date^1")
                });
            });
    });

    it(`doesn't explode if users enter silly things`, () => {
        return claudia
            .simulateSlackCommand('plz ducks', 'EA333')
            .then(response => {
                // Expect error
                expect(response).toEqual({
                    text: "I wasn't able to understand what time you are trying to request for lunch. Please specify a time in the format HH:MM am/pm."
                });
            });
    });

    it(`responds with wat`, () => {
        return claudia
            .simulateSlackCommand('tacos plz', 'EA333')
            .then(response => {
                // Expect error
                expect(response).toEqual({
                    text: "I'm sorry, I don't understand \"tacos plz\" . Check `/supportlunch what` for a list of valid commands."
                });
            });
    });

    it('works if two heros are scheduled at the same time', () => {
        if (!verifyIsEarlyEnoughToSchedule()) return;

        return claudia
            .simulateSlackCommand('plz 11:30pm', 'EA333')
            .then(response => {
                // Expect reply OK
                expect(response).toEqual({
                    text: expect.stringContaining("Okay <@EA333>, I have you scheduled for <!date^1")
                });
            })
            .then(() => claudia.simulateSlackCommand('plz 11:30pm', 'NK422'))
            .then(response => {
                // Expect reply OK
                expect(response).toEqual({
                    text: expect.stringContaining("Okay <@NK422>, I have you scheduled for <!date^1")
                });
            })
            .then(() => claudia.simulateSlackCommand('plz 11:01pm', 'TT222'))
            .then(response => {
                // Expect reply NO
                expect(response).toEqual({
                    text: expect.stringContaining("I'm sorry, <@TT222>. There will be more than two heroes")
                });
            })
    });

});
