const {
    canHeroGo,
    _isOverlap,
    _getAWOLHeroes
} = require('./businesslogic');

const {
    referenceTimestamp,
    minutes,
} = require('./test-fixtures/reference-time');

describe('canHeroGo', () => {

    it('returns false if two heroes are currently out', () => {
        const exampleDataTwoCurrentlyOut = {
            // Item: {
                // heroes: {
                    Leez: { isOut: true, timeOut: referenceTimestamp - minutes(15) },
                    noover: { isOut: true, timeOut: referenceTimestamp - minutes(20) },
            //     }
            // }
        };

        expect(
            canHeroGo(exampleDataTwoCurrentlyOut, referenceTimestamp)
        ).toEqual(
            false
        );
    });

    it('returns true if only one hero is currently out', () => {
        const exampleDataOneCurrentlyOut = {
            // Item: {
            //     heroes: {
                    Leez: { isOut: true, timeOut: referenceTimestamp - minutes(15) }
            //     }
            // }
        };

        expect(
            canHeroGo(exampleDataOneCurrentlyOut, referenceTimestamp)
        ).toEqual(
            true
        );
    });

    it('ignores heroes who are already back', () => {
        const exampleDataOneOutOneBack = {
            // Item: {
            //     // One out, one already back
            //     heroes: {
                    Leez: { isOut: true, timeOut: referenceTimestamp - minutes(15) },
                    naat: { isOut: false, timeOut: referenceTimestamp - minutes(35), timeBack: referenceTimestamp - minutes(5) }
            //     }
            // }
        };

        expect(
            canHeroGo(exampleDataOneOutOneBack, referenceTimestamp)
        ).toEqual(
            true
        );
    });

    it('ignores heroes who are scheduled >=30 minutes after the requested time out', () => {
        const exampleDataOneOutOneScheduled = {
            // Item: {
            //     // One out, one scheduled
            //     heroes: {
                    Leez: { isOut: true, timeOut: referenceTimestamp - minutes(15) },
                    naat: { isOut: false, timeOut: referenceTimestamp + minutes(35) }
            //     }
            // }
        };

        expect(
            canHeroGo(exampleDataOneOutOneScheduled, referenceTimestamp)
        ).toEqual(
            true
        );
    });

    it('keeps its shit together when dealing with larger amounts of data', () => {
        const exampleDataOneOutOneScheduled = {
            // Item: {
            //     heroes: {
                    // Already back
                    Leez:   { isOut: false, timeOut: referenceTimestamp - minutes(115), timeBack: referenceTimestamp - minutes(85) },
                    naat:   { isOut: false, timeOut: referenceTimestamp - minutes(110), timeBack: referenceTimestamp - minutes(80) },
                    topper: { isOut: false, timeOut: referenceTimestamp - minutes(35), timeBack: referenceTimestamp - minutes(5) },
                    // Out
                    meow:   { isOut: true, timeOut: referenceTimestamp - minutes(15) },
                    // Scheduled
                    tylerdad:   { isOut: false, timeOut: referenceTimestamp + minutes(20) },
                    anonymous:  { isOut: false, timeOut: referenceTimestamp + minutes(55) },
                    meowstifer: { isOut: false, timeOut: referenceTimestamp + minutes(55) }
            //     }
            // }
        };

        expect(
            canHeroGo(exampleDataOneOutOneScheduled, referenceTimestamp)
        ).toEqual(
            true
        );
    });

    it('returns true if one hero is out and one is scheduled, and their times do not overlap', () => {
        const exampleDataTwoOutNotOverlapping = {
            // Item: {
            //     // One out, one scheduled (no overlap)
            //     heroes: {
                    Leez: { isOut: true, timeOut: referenceTimestamp - minutes(15) },
                    naat: { isOut: false, timeOut: referenceTimestamp + minutes(20) }
            //     }
            // }
        };

        expect(
            canHeroGo(exampleDataTwoOutNotOverlapping, referenceTimestamp)
        ).toEqual(
            true
        );
    });

    it('returns false if one hero is out and one is scheduled, and their times overlap (one more would make three)', () => {
        const exampleDataTwoOutOverlapping = {
            // Item: {
            //     // One out, one scheduled (with overlap)
            //     heroes: {
                    Leez: { isOut: true, timeOut: referenceTimestamp - minutes(15) },
                    naat: { isOut: false, timeOut: referenceTimestamp + minutes(10) }
            //     }
            // }
        };

        expect(
            canHeroGo(exampleDataTwoOutOverlapping, referenceTimestamp)
        ).toEqual(
            false
        );
    });

    it('does\'t blow up if two heroes schedule lunch at the same time', () => {
        const exampleDataTwoOutOverlapping = {
            // "Item": {
            //     "heroes": {
                    NK422:{ timeBack:0, isOut:false, timeOut: referenceTimestamp - minutes(30) },
                    EA333:{ timeBack:0, isOut:false, timeOut: referenceTimestamp - minutes(30) }
            //     }
            // }
        };

        expect(
            canHeroGo(exampleDataTwoOutOverlapping, referenceTimestamp)
        ).toEqual(
            true
        );
    });

    it('returns false if two heroes are AWOL', () => {
        const exampleDataAWOLDuo = {
            // Item: {
            //     // Two AWOL
            //     heroes: {
                    // AWOL
                    naat: { isOut: true, timeOut: referenceTimestamp - minutes(45) },
                    // AWOL
                    Leez: { isOut: true, timeOut: referenceTimestamp - minutes(44) },
            //     }
            // }
        };
        // hmmm
        const message = "now";

        expect(
            canHeroGo(exampleDataAWOLDuo, referenceTimestamp, message)
        ).toEqual(
            false
        );
    });

    it('returns false if one hero is AWOL and one hero is out (one more would make three)', () => {
        const exampleDataAWOLOverlap = {
            // Item: {
            //     // One AWOL, one out
            //     heroes: {
                    // AWOL
                    naat: { isOut: true, timeOut: referenceTimestamp - minutes(45) },
                    // At lunch
                    nooover: { isOut: true, timeOut: referenceTimestamp - minutes(10) },
            //     }
            // }
        };
        // grr
        const message = "now";

        expect(
            canHeroGo(exampleDataAWOLOverlap, referenceTimestamp, message)
        ).toEqual(
            false
        );
    });

    it('returns true if one hero is AWOL, and one scheduled within 30 minutes of requested time out', () => {
        const exampleDataAWOLNoOverlap = {
            // Item: {
            //     // One AWOL, one scheduled
            //     heroes: {
                    // AWOL
                    naat: { isOut: true, timeOut: referenceTimestamp - minutes(35) },
                    // Scheduled
                    nooover: { isOut: false, timeOut: referenceTimestamp + minutes(10) },
            //     }
            // }
        };
        // mmh
        const message = "now";

        expect(
            canHeroGo(exampleDataAWOLNoOverlap, referenceTimestamp, message)
        ).toEqual(
            true
        );
    });

});

describe('getAWOLHeroes', () => {

    it('returns an array of AWOL heroes', () => {
        const heroListWithAWOL = [
            // AWOL
            {isOut: true, timeOut:referenceTimestamp - minutes(40)},
            // Out (but not overdue)
            {isOut: true, timeOut:referenceTimestamp - minutes(10)}
        ];
        expect(
            _getAWOLHeroes(heroListWithAWOL, referenceTimestamp)
        ).toEqual(
            [{isOut: true, timeOut:referenceTimestamp - minutes(40)}]
        );
    });

    it('returns an empty array if no heroes are AWOL', () => {
        const heroListNoAWOL = [
            // Already back
            {isOut: false, timeOut:referenceTimestamp - minutes(40), timeBack: referenceTimestamp - minutes(10)},
            // Out (but not overdue)
            {isOut: true, timeOut:referenceTimestamp - minutes(20)}
        ];

        expect(
            _getAWOLHeroes(heroListNoAWOL, referenceTimestamp)
        ).toEqual(
            []
        );
    });

});

describe('isOverlap', () => {

    it('should return false if there is no overlap', () => {
        const hero1 = {
            timeOut: referenceTimestamp - minutes(16),
            isOut: true
        };
        const hero2 = {
            timeOut: referenceTimestamp + minutes(15),
            isOut: false
        };

        expect(
            _isOverlap(hero1, hero2)
        ).toEqual(
            false
        );

        expect(
            _isOverlap(hero2, hero1)
        ).toEqual(
            false
        );
    });

    it('should return true if there is overlap', () => {
        const hero1 = {
            timeOut: referenceTimestamp - minutes(15),
            isOut: true
        };
        const hero2 = {
            timeOut: referenceTimestamp + minutes(5),
            isOut: false
        };

        expect(
            _isOverlap(hero1, hero2)
        ).toEqual(
            true
        );

        expect(
            _isOverlap(hero2, hero1)
        ).toEqual(
            true
        );
    });

    it('only concerns itself with scheduled/theoretical overlap, ignores AWOL heroes', () => {
        // no scheduled overlap between times, but hero1 is AWOL
        const hero1 = {
            timeOut: referenceTimestamp - minutes(35),
            isOut: true
        };
        const hero2 = {
            timeOut: referenceTimestamp - minutes(4),
            isOut: true
        };

        expect(
            _isOverlap(hero1, hero2)
        ).toEqual(
            false
        );
    });

});
