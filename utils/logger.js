// Specify individual module names, or `true` to log message from all modules
const logMessages = [
    // 'db',
    'controller',
    // 'dataparser',
    'businesslogic',
    'stringtotimestamp',
    'lambda',
];
const logInfo = [
    'stringtotimestamp.test',
];
const logErrors = false;

const shouldLog = (should, maybe) => should === true || should instanceof Array && should.includes(maybe);

const getLogFunction = (moduleName) => shouldLog(logMessages, moduleName)
    ? console.log.bind(console)
    : () => {};

const getInfoFunction = (moduleName) => shouldLog(logInfo, moduleName)
    ? console.info.bind(console)
    : () => {};

const getErrorFuncton = (moduleName) => shouldLog(logErrors, moduleName)
    ? console.error.bind(console)
    : () => {};

module.exports = (moduleName = '') => ({
    log: getLogFunction(moduleName),
    info: getInfoFunction(moduleName),
    error: getErrorFuncton(moduleName),
});
